<?php

/**
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BnLibrariesBundle
 * @license    LGPL-3.0+
 * @see	       https://gitlab.com/srhinow/bn-libraries-bundle
 *
 */

namespace Srhinow\BnLibrariesBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;

use Srhinow\BnLibrariesBundle\SrhinowBnLibrariesBundle;

/**
 * Plugin for the Contao Manager.
 *
 * @author Sven Rhinow
 */
class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(SrhinowBnLibrariesBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
