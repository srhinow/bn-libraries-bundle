<?php
/**
* get geodata from OSM-API
*/

namespace Srhinow\BnLibrariesBundle\Libs;

use GuzzleHttp\Client;

class OsmGeoData
{
    private $url = "https://nominatim.openstreetmap.org/search?q=##ADDRESS##&format=jsonv2&countrycodes=de&addressdetails=1&limit=1";
    private $server = false;
    public $geoData = null;

    public function __construct($query=false)
    {
         $this->setServer();
         if($query) return $this->getGeoData($query);
    }

    public function setServer($s=false)
    {
		if(!$this->server) $this->server = 'http://'.$_SERVER['HTTP_HOST'];
		if($s) $this->server = $s;
    }

    /**
    * get geo-data for a location-string
    * @var string
    * @return json-string
    */
    public function getGeoData($query)
    {

        $address = trim($query['address']);

        $url = str_replace('##ADDRESS##',$address,$this->url);
//        if(strlen($query['plz']) > 0) $url .= '&postalcode='.$query['plz'];

        $client = new Client();

        $response = $client->request("GET", $url, [
            "headers" => [
                "User-Agent" => "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)",
                "Accept" => "application/json",
                "Content-type" => "application/json"
                ]
            ]
        );
        $result = json_decode($response->getBody()->getContents());

		return $result;
    }

}
