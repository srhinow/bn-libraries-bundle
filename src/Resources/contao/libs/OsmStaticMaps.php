<?php
/**
 * get static-map-image from MapBox-API
 * API-DOC: https://www.mapbox.com/api-documentation/#static
 * URI-Genrerator: https://staticmapmaker.com/mapbox/
 */

namespace Srhinow\BnLibrariesBundle\Libs;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class OsmStaticMaps
{
    // OLD
    //https://api.mapbox.com/v4/mapbox.streets/pin-m-marker+f33e3e(9.9154078,53.2902769)/9.9154078,53.2902769,14/200x200.jpg70?access_token=pk.eyJ1Ijoic3JoaW5vdyIsImEiOiJjanBlb2swYWgwMjhtM3ZsZ24xY3VrNXNuIn0.sAoohGp06nmqN9kAcc-A8A
    // private $url = "https://api.mapbox.com/v4/mapbox.streets/pin-m-marker+%s(%s)/%s,%s/%sx%s%s?access_token=%s";
    // NEW
    // https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/pin-s+f33e3e(7.6071,53.6508)/7.6071,53.6508,14.66,0/250x250?access_token=pk.eyJ1Ijoic3JoaW5vdyIsImEiOiJjanBlb2ZkbHkwMmNhM3dzMHJvOWd2bTFiIn0.ndNXM6XNPznKINfO2wS04g
    private $url = "https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/pin-s+%s(%s)/%s,%s,0/%sx%s?access_token=%s";
    private $server = false;
    private $access_token = 'pk.eyJ1Ijoic3JoaW5vdyIsImEiOiJjanBlb2ZkbHkwMmNhM3dzMHJvOWd2bTFiIn0.ndNXM6XNPznKINfO2wS04g';
    private $options = [];

    public function __construct($dest=false)
    {
        $this->setServer();
        $this->setDefaultOptions();
        if($dest) return $this->getAndSaveStaticMapImage($dest);
    }

    public function setServer($s=false)
    {
        if(!$this->server) $this->server = 'http://'.$_SERVER['HTTP_HOST'];
        if($s) $this->server = $s;
    }

    private function setDefaultOptions() {
        $this->options = [
            'pin' => 'f33e3e',
            'marker_coordinates' => '9.9154078,53.2902769',
            'coordinates' => '9.9154078,53.2902769',
            'zoom' => 15,
            'width' => 250,
            'height' => 250,
            'format' => '.png',
            'access_token' => $this->access_token
        ];
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    public function createApiUrl(){

        $o = $this->options;
        $r = sprintf($this->url,
            $o['pin'],
            $o['marker_coordinates'],
            $o['coordinates'],
            $o['zoom'],
            $o['width'],
            $o['height'],
            $o['access_token']
        );

        return $r;
    }

    public function getAndSaveStaticMapImage($dest,$options = null){

        if($options && is_array($options)) $this->setOptions($options);

        $url = $this->createApiUrl();

        $client = new Client();

        try {
            $client->get($url,['save_to'=>TL_ROOT.'/'.$dest]);
        } catch (RequestException $e) {
            echo $e->getRequest() . "\n";
            if ($e->hasResponse()) {
                echo $e->getResponse() . "\n";
            }
        }

        return $dest;
    }
}
