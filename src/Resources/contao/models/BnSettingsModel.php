<?php

/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Srhinow\BnLibrariesBundle\Models;

use Contao\Model;

class BnSettingsModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_bn_settings';

	/**
	 * Find published settings items by their parent ID and ID or alias
	 *
	 * @param array $arrOptions An optional options array
	 *
	 * @return \Model|null The NewsModel or null if there are no news
	 */
	public static function findFirstSettings( array $arrOptions=array())
	{
		$t = static::$strTable;
        $arrOptions['limit'] = 1;
		return static::find( $arrOptions);
	}	

}
