<?php

/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Srhinow\BnLibrariesBundle\Models;

use Contao\Model;

class BnLeitungModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_bn_leitung';

}
