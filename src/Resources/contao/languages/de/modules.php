<?php

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2013 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Leo Feyer 2005-2013
 * @author     Leo Feyer <https://contao.org>
 * @package    Language
 * @license    LGPL
 * @filesource
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['libraries'][0]     = 'Bibliotheken';
$GLOBALS['TL_LANG']['MOD']['libraries'][1] = 'Verwalten Sie hier alle verfügbaren Bibliotheken in Niedersachsen.';
$GLOBALS['TL_LANG']['MOD']['bn']  = 'Bibliotheken in NS';
$GLOBALS['TL_LANG']['MOD']['tl_bn_setup'][0] = 'Verwaltung';
$GLOBALS['TL_LANG']['MOD']['tl_bn_setup'][1] = 'Verwalten Sie die Einstellungen zur Bibliotheksverwaltung.';
 
/**
 * Front end modules
 */
 $GLOBALS['TL_LANG']['FMD']['bn_libs']      = 'BN Bibliotheken';
 $GLOBALS['TL_LANG']['FMD']['bn_search_form'] = array('Such-Formular', '');
 $GLOBALS['TL_LANG']['FMD']['bn_search_list'] = array('Suchergebnis-Liste', '');
 $GLOBALS['TL_LANG']['FMD']['bn_search_map'] = array('Suchergebnis-Karte', '');
 $GLOBALS['TL_LANG']['FMD']['bn_details'] = array('Detailansicht', '');
 $GLOBALS['TL_LANG']['FMD']['bn_edit_entry'] = array('Bearbeitungsansicht', 'nur für den geschlossenen Bereich. Damit die jeweiligen Biblitoheekn ihren Datensatz pflegen können.');

/**
 * settings Modules
 */
$GLOBALS['TL_LANG']['BN']['config_module']             = 'Bibliotheken Niedersachsen Konfiguration (Version: %s)';

$GLOBALS['TL_LANG']['IMD']['config']                    = 'Verwaltung';
$GLOBALS['TL_LANG']['IMD']['bn_leitung'][0]             = 'Leitung';
$GLOBALS['TL_LANG']['IMD']['bn_leitung'][1]             = 'Verwalten Sie die Leitungsarten, welche einer Bibliothek zugeordnet werden kann.';
$GLOBALS['TL_LANG']['IMD']['bn_traeger'][0]             = 'Träger der Bibliothek';
$GLOBALS['TL_LANG']['IMD']['bn_traeger'][1]             = 'Verwalten Sie die Träger, welche einer Bibliothek zugeordnet werden kann.';
$GLOBALS['TL_LANG']['IMD']['bn_leistungen'][0]          = 'Leistungen';
$GLOBALS['TL_LANG']['IMD']['bn_leistungen'][1]          = 'Verwalten Sie die verschiedenen Leistungen welche einer Bibliothek zugeordnet werden kann.';
$GLOBALS['TL_LANG']['IMD']['bn_medien'][0]            	= 'Medienarten';
$GLOBALS['TL_LANG']['IMD']['bn_medien'][1]            	= 'Verwalten Sie die Medienarten welche einer Bibliothek zugeordnet werden kann.';
$GLOBALS['TL_LANG']['IMD']['bn_settings'][0]            = 'Einstellungen';
$GLOBALS['TL_LANG']['IMD']['bn_settings'][1]            = 'Hier legen Sie allgemeine Einstellungen der Bibliotheksverwaltung fest.';
