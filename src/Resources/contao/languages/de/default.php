<?php
 
$GLOBALS['TL_LANG']['MSC']['tags']['0'] = "Schlagworte";
$GLOBALS['TL_LANG']['MSC']['tags']['1'] = "Bitte geben Sie eine oder mehrere Schlagworte durch Komma getrennt an, um eine Kategorisierung vorzunehmen.";
$GLOBALS['TL_LANG']['MSC']['apply']            = 'Aktualisieren'; 
$GLOBALS['TL_LANG']['MSC']['emptyBnList'] = 'Es sind keine Bibliotheken mit diesen Eigenschaften vorhanden.';
