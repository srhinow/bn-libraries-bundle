<?php
/**
 * TL_ROOT/system/modules/beekeeping_bee_pasture/languages/de/tl_bn_leitung.php
 *
 * Contao extension: beekeeping_bee_pasture
 * Deutsch translation file
 *
 * Copyright : &copy; Sven Rhinow <sven@sr-tag.de>
 * License   : LGPL
 * Author    : Sven Rhinow, http://www.sr-tag.de/
 * Translator: Sven Rhinow (scuM666)
 *
 * This file was created automatically be the TYPOlight extension repository translation module.
 * Do not edit this file manually. Contact the author or translator for this module to establish
 * permanent text corrections which are update-safe.
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_bn_leitung']['name'][0]         = 'Name';
$GLOBALS['TL_LANG']['tl_bn_leitung']['name'][1]         = 'Name der Leitung';
$GLOBALS['TL_LANG']['tl_bn_leitung']['alias'][0]			= 'Alias';
$GLOBALS['TL_LANG']['tl_bn_leitung']['alias'][1]        	= 'wird z.B. für die URL-Übergabe benötigt';
$GLOBALS['TL_LANG']['tl_bn_leitung']['sorting'][0]		= 'Sortierung';
$GLOBALS['TL_LANG']['tl_bn_leitung']['sorting'][1]		= 'Sortierungsplatz in einer eventuelle Listen-Darstellung';

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_bn_leitung']['new'][0]                          = 'Neue Leitung';
$GLOBALS['TL_LANG']['tl_bn_leitung']['new'][1]                          = 'Eine neue Leitung anlegen.';
$GLOBALS['TL_LANG']['tl_bn_leitung']['edit'][0]                         = 'Leitung bearbeiten';
$GLOBALS['TL_LANG']['tl_bn_leitung']['edit'][1]                         = 'Leitung ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_bn_leitung']['copy'][0]                         = 'Leitung duplizieren';
$GLOBALS['TL_LANG']['tl_bn_leitung']['copy'][1]                         = 'Leitung ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_bn_leitung']['delete'][0]                       = 'Leitung löschen';
$GLOBALS['TL_LANG']['tl_bn_leitung']['delete'][1]                       = 'Leitung ID %s löschen.';
$GLOBALS['TL_LANG']['tl_bn_leitung']['show'][0]                         = 'Leitungdetails anzeigen';
$GLOBALS['TL_LANG']['tl_bn_leitung']['show'][1]                         = 'Details für Leitung ID %s anzeigen.';

