<?php
/**
 * TL_ROOT/system/modules/bn_libraries/languages/de/tl_bn_medien.php
 *
 * Contao extension: bn_libraries
 * german translation file
 *
 * @copyright : &copy; Sven Rhinow <sven@sr-tag.de>
 * @license    commercial
 * @author    : Sven Rhinow, http://www.sr-tag.de/
 *
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_bn_libraries']['bibliotheksname']      = array('Name der Bibliothek', 'Geben Sie hier den Namen der Bibliothek an.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['zweigstellenname']      = array('Name der Zweigstelle', 'Falls vorhanden geben Sie hier die Bezeichnung der Zweigstelle ein.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['leiter_name']       = array('Name d. Leiters/Leiterin', '');
$GLOBALS['TL_LANG']['tl_bn_libraries']['leitung']       = array('Leitung', 'Wie wird die Bibliothek geleitet?');
$GLOBALS['TL_LANG']['tl_bn_libraries']['traeger']       = array('Träger der Bibliothek', 'Durch wen wird die Bibliothek getragen?');
$GLOBALS['TL_LANG']['tl_bn_libraries']['strasse']      = array('Straße', 'Bitte geben Sie den Straßennamen ohne die Hausnummer ein.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['hausnummer']      = array('Hausnummer', 'Bitte geben Sie hier die Hausnummer ein.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['plz']      = array('Postleitzahl', 'Bitte geben Sie die Postleitzahl ein.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['ort']        = array('Ort', 'Bitte geben Sie den Namen des Ortes ein.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['stadtteil'] = array('Stadtteil', 'Hier können Sie angeben in welchem Stadtteil sich die Bibliothek befindet.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['gemeinde'] = array('Gemeinde', 'Hier können Sie angeben in welcher Gemeinde sich die Bibliothek befindet.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['landkreis'] = array('Landkreis', 'Hier können Sie angeben in welchem Landkreis sich die Bibliothek befindet.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['bezirk'] = array('Bezirk', 'Hier können Sie angeben in welchem Bezirk sich die Bibliothek befindet.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['telefon']       = array('Telefonnummer', 'Bitte geben Sie die Telefonnummer ein.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['fax']         = array('Faxnummer', 'Bitte geben Sie die Faxnummer ein.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['email']   = array('E-Mail-Adresse', 'Bitte geben Sie die E-Mail-Adresse des Abonnenten ein.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['website']     = array('Webseite', 'Hier können Sie eine Web-Adresse eingeben.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['webkatalog']     = array('Webkatalog', 'Hier können Sie die Webkatalog-Adresse eingeben.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['onleihe']     = array('Onleihe', 'Hier können Sie die Onleihe-Adresse eingeben.');

$GLOBALS['TL_LANG']['tl_bn_libraries']['blog']     = array('Blog', 'Hier können Sie die Blog-URL eingeben.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['facebook']     = array('Facebook', 'Hier können Sie die Facebook-Adresse eingeben.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['gplus']     = array('G+ (Google Plus)', 'Hier können Sie die Google Plus-Adresse eingeben.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['twitter']     = array('Twitter', 'Hier können Sie die Twitter-Adresse eingeben.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['instagram']     = array('Instagram', 'Hier können Sie die Instagram-Adresse eingeben.');

$GLOBALS['TL_LANG']['tl_bn_libraries']['libImages']     = array('Bilder der Bibliothek', 'Hier können Sie Bilder für diese bibliothek zuordnen.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['medienbestand']     = array('Medienbestand', 'Wieviel hMedien hat diese Bibliothek?');
$GLOBALS['TL_LANG']['tl_bn_libraries']['lat']     = array('Latitude ', 'geben sie hier die Breitenangabe als GEO-Wert ein');
$GLOBALS['TL_LANG']['tl_bn_libraries']['lon']     = array('Longitude ', 'geben sie hier die Längennangabe als GEO-Wert ein');
$GLOBALS['TL_LANG']['tl_bn_libraries']['setnewgeo']     = array('Geodaten neu setzen', 'Wenn sie die Adressdaten geändert haben können sie hiermit die GEo-Koordinaten neu netzen lassen.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['mo_1_von']     = array('Montag (1) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['mo_1_bis']     = array('Montag (1) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['mo_2_von']     = array('Montag (2) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['mo_2_bis']     = array('Montag (2) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['di_1_von']     = array('Dienstag (1) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['di_1_bis']     = array('Dienstag (1) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['di_2_von']     = array('Dienstag (2) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['di_2_bis']     = array('Dienstag (2) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['mi_1_von']     = array('Mittwoch (1) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['mi_1_bis']     = array('Mittwoch (1) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['mi_2_von']     = array('Mittwoch (2) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['mi_2_bis']     = array('Mittwoch (2) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['do_1_von']     = array('Donnerstag (1) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['do_1_bis']     = array('Donnerstag (1) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['do_2_von']     = array('Donnerstag (2) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['do_2_bis']     = array('Donnerstag (2) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['fr_1_von']     = array('Freitag (1) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['fr_1_bis']     = array('Freitag (1) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['fr_2_von']     = array('Freitag (2) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['fr_2_bis']     = array('Freitag (2) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['sa_1_von']     = array('Samstag (1) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['sa_1_bis']     = array('Samstag (1) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['sa_2_von']     = array('Samstag (2) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['sa_2_bis']     = array('Samstag (2) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['so_1_von']     = array('Sonntag (1) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['so_1_bis']     = array('Sonntag (1) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['so_2_von']     = array('Sonntag (2) geöffnet von', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['so_2_bis']     = array('Sonntag (2) geöffnet bis', 'Geben Sie die Uhrzeit in dem Format hh:mm an z.B. 09:00.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['medien']     = array('Medien', 'Welche Arten von Medien hat diese Bibliothek?');
$GLOBALS['TL_LANG']['tl_bn_libraries']['sonstmedien']     = array('sonstige Medien', 'Welche Arten von Medien haben Sie außerdem?');
$GLOBALS['TL_LANG']['tl_bn_libraries']['leistungen']     = array('Leistungen', 'Welche Arten von Leistungen hat diese Bibliothek?');
$GLOBALS['TL_LANG']['tl_bn_libraries']['sonstleistungen']     = array('sonstige Leistungen', 'Welche Arten von Leistungen haben Sie außerdem?');
$GLOBALS['TL_LANG']['tl_bn_libraries']['image_1']     = array('Bild 1', 'Das erste Bild wird in den Biblitoheksdetails angezeigt.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['image_2']     = array('Bild 2', 'Dieses Bild wird nur in der vergrößersten Gallerie angezeigt.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['image_3']     = array('Bild 3', 'Dieses Bild wird nur in der vergrößersten Gallerie angezeigt.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['image_4']     = array('Bild 4', 'Dieses Bild wird nur in der vergrößersten Gallerie angezeigt.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['image_5']     = array('Bild 5', 'Dieses Bild wird nur in der vergrößersten Gallerie angezeigt.');
$GLOBALS['TL_LANG']['tl_bn_libraries']['memo']     = array('Memo / Notizen', 'Hier können Sie weitere Informationen eingeben.');

/**
 * CSV-Export
 */
$GLOBALS['TL_LANG']['tl_bn_libraries']['csvLibraryExport'] = array('CSV-Export', '');
$GLOBALS['TL_LANG']['tl_bn_libraries']['button_exportcsv'] = 'CSV generieren';
$GLOBALS['TL_LANG']['tl_bn_libraries']['h2_exportcsv'] = 'Bibliotheken in eine CSV exportieren';
/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_bn_libraries']['new']        = array('Neue Bibliothek', 'Eine neue Bibliothek anlegen');
$GLOBALS['TL_LANG']['tl_bn_libraries']['show']       = array('Buchungdetails', 'Details der Bibliothek ID %s anzeigen');
$GLOBALS['TL_LANG']['tl_bn_libraries']['edit']       = array('Bibliothek bearbeiten', 'Bibliothek ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_bn_libraries']['editheader'] = array('Bibliothek-Einstellungen bearbeiten', 'Einstellungen der Bibliothek ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_bn_libraries']['copy']       = array('Bibliothek duplizieren', 'Bibliothek ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_bn_libraries']['cut']        = array('Bibliothek verschieben', 'Bibliothek ID %s verschieben');
$GLOBALS['TL_LANG']['tl_bn_libraries']['delete']     = array('Bibliothek löschen', 'Bibliothek ID %s löschen');
$GLOBALS['TL_LANG']['tl_bn_libraries']['toggle']     = array('Bibliothek veröffentlichen/unveröffentlichen', 'Bibliothek ID %s veröffentlichen/unveröffentlichen');

/**
* Legends
*/
$GLOBALS['TL_LANG']['tl_bn_libraries']['main_legend'] = "Grunddaten";
$GLOBALS['TL_LANG']['tl_bn_libraries']['address_legend'] = "Adressdaten";
$GLOBALS['TL_LANG']['tl_bn_libraries']['contact_legend'] = "Kontaktdaten";
$GLOBALS['TL_LANG']['tl_bn_libraries']['openingtimes_legend'] = "Öffnungszeiten";
$GLOBALS['TL_LANG']['tl_bn_libraries']['extend_legend'] = "erweiterte Einstellungen";
$GLOBALS['TL_LANG']['tl_bn_libraries']['medien_legend'] = "Medien-Einstellungen";
$GLOBALS['TL_LANG']['tl_bn_libraries']['leistungen_legend'] = "Leistungen-Einstellungen";
$GLOBALS['TL_LANG']['tl_bn_libraries']['images_legend'] = "Bilder der Bibliothek";
$GLOBALS['TL_LANG']['tl_bn_libraries']['geodata_legend'] = "Geo-Koordinaten";

