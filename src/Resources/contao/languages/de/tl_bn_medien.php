<?php
/**
 * TL_ROOT/system/modules/bn_libraries/languages/de/tl_bn_medien.php
 *
 * Contao extension: bn_libraries
 * german translation file
 *
 * @copyright : &copy; Sven Rhinow <sven@sr-tag.de>
 * @license    commercial
 * @author    : Sven Rhinow, http://www.sr-tag.de/
 *
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_bn_medien']['name'][0]         = 'Name';
$GLOBALS['TL_LANG']['tl_bn_medien']['name'][1]         = 'Name vom Medium';
$GLOBALS['TL_LANG']['tl_bn_medien']['alias'][0]			= 'Alias';
$GLOBALS['TL_LANG']['tl_bn_medien']['alias'][1]        	= 'wird z.B. für die URL-Übergabe benötigt';
$GLOBALS['TL_LANG']['tl_bn_medien']['sorting'][0]		= 'Sortierung';
$GLOBALS['TL_LANG']['tl_bn_medien']['sorting'][1]		= 'Sortierungsplatz in einer eventuelle Listen-Darstellung';

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_bn_medien']['new'][0]                          = 'Neues Medium';
$GLOBALS['TL_LANG']['tl_bn_medien']['new'][1]                          = 'Ein neues Medium anlegen.';
$GLOBALS['TL_LANG']['tl_bn_medien']['edit'][0]                         = 'Medium bearbeiten';
$GLOBALS['TL_LANG']['tl_bn_medien']['edit'][1]                         = 'Medium ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_bn_medien']['copy'][0]                         = 'Medium duplizieren';
$GLOBALS['TL_LANG']['tl_bn_medien']['copy'][1]                         = 'Medium ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_bn_medien']['delete'][0]                       = 'Medium löschen';
$GLOBALS['TL_LANG']['tl_bn_medien']['delete'][1]                       = 'Medium ID %s löschen.';
$GLOBALS['TL_LANG']['tl_bn_medien']['show'][0]                         = 'Mediumdetails anzeigen';
$GLOBALS['TL_LANG']['tl_bn_medien']['show'][1]                         = 'Details für Medium ID %s anzeigen.';

