<?php

/*
 * This file is part of bn-libraries-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Srhinow\BnLibrariesBundle\Modules;


use Contao\Database;
use Contao\Files;
use Contao\Module;
use Contao\Session;
use Srhinow\BnLibrariesBundle\Libs\OsmGeoData;
use Srhinow\BnLibrariesBundle\Libs\OsmStaticMaps;

/**
 * Class ModuleBn
 */
abstract class ModuleBn extends Module
{

	/**
	 * Parse an item and return it as string
	 * @param object
	 * @param string
	 * @param integer
	 * @return string
	 */
	protected function parseLibrary($objLib, $strClass='', $intCount=0)
	{
		global $objPage;

		$objTemplate = new \FrontendTemplate($this->item_template);
		$objTemplate->setData($objLib->row());
		$objTemplate->class = (($objLib->cssClass != '') ? ' ' . $objLib->cssClass : '') . $strClass;
		$objTemplate->openStatus = $this->getCurrentOpenStatus($objLib);

		//Detail-Url
		if($this->jumpTo)
		{
			$objDetailPage = \PageModel::findByPk($this->jumpTo);
			$objTemplate->detailUrl = ampersand( $this->generateFrontendUrl($objDetailPage->row(),'/lib/'.$objLib->id) );
		}

		return $objTemplate->parse();
	}


	/**
	 * Parse one or more items and return them as array
	 * @param object
	 * @return array
	 */
	protected function parseLibraries($objLibs)
	{
		$limit = $objLibs->count();
		// print $limit;
		if ($limit < 1)
		{
			return array();
		}

		$count = 0;
		$arrLibs = array();

		while ($objLibs->next())
		{
			$distance = $this->getDistance($objLibs);
			if(strlen($distance)>0)
			{
				$arrLibs[$distance] = array
				(
					'data' => $objLibs->row(),
					'distance' => $distance,
					'html' => $this->parseLibrary($objLibs, ((++$count == 1) ? ' first' : '') . (($count == $limit) ? ' last' : '') . ((($count % 2) == 0) ? ' odd' : ' even'), $count)
				);
			}
			else
			{
				$arrLibs[] = array
				(
					'data' => $objLibs->row(),
					'distance' => $distance,
					'html' => $this->parseLibrary($objLibs, ((++$count == 1) ? ' first' : '') . (($count == $limit) ? ' last' : '') . ((($count % 2) == 0) ? ' odd' : ' even'), $count)
				);
			}
		}

		ksort($arrLibs);
		return $arrLibs;
	}

	/**
	* get status if library current open
	* @param object
	* @return string
	*/
	protected function getCurrentOpenStatus($objLib)
	{
		$status = '';
		$currTime = time();
		$wdArr = array(1=>'mo', 2=>'di', 3=>'mi', 4=>'do', 5=>'fr', 6=>'sa', 7=>'so');
		$wd = $wdArr[date('N')];
		for($i=1 ; $i <= 2 ; $i++)
		{
			if($status == 'open') continue;

			$wd_von = $wd.'_'.$i.'_von';
			$wd_vonTime = mktime(date('H', $objLib->$wd_von) , date('i',$objLib->$wd_von) , date('s',$objLib->$wd_von) , date('n') , date('j') , date('Y'));

			$wd_bis = $wd.'_'.$i.'_bis';
			$wd_bisTime = mktime(date('H',$objLib->$wd_bis) , date('i',$objLib->$wd_bis) , date('s',$objLib->$wd_bis) , date('n') , date('j') , date('Y'));

			if((int) $objLib->$wd_von == 0 || (int) $objLib->$wd_bis == 0) $status =  'close' ;

			if(((int) $objLib->$wd_von != 0) && !in_array($status,array('-','open')) )
			{
				$status = ($wd_vonTime <= $currTime && $wd_bisTime >= $currTime) ? 'open' : 'close';
			}
		}

		return $status;
	}

	/**
	* return lat and lon from given location in the session
	* @param string
	* @return array
	*/
	public function getGeoDataFromCurrentPosition($searchVal='')
	{
		$session = Session::getInstance()->get('bnfilter');
        $plzcity = '';
        $geoData = array();

        if(isset($session) && array_key_exists('plzcity',$session)) $plzcity = $session['plzcity'];
        if(strlen($searchVal)>0) $plzcity = $searchVal;
		$plzcity = trim($plzcity);

		if(strlen($plzcity) < 1) return $geoData;

		$geo = new OsmGeoData();
		$searchData = $this->splitAdressString($plzcity);
    	$arrJson = $geo->getGeoData($searchData);

        if (is_array($arrJson) && is_object($arrJson[0])) {

            $geodata['lat'] = $arrJson[0]->lat;
            $geodata['lon'] = $arrJson[0]->lon;
            $geodata['plzcity'] = $searchData['string'];

            foreach ($arrJson as $json) {
                if ($json->addresstype !== 'town') {
                    continue;
                }

                $geodata['lat'] = $json->lat;
                $geodata['lon'] = $json->lon;
            }
        }

	    return $geodata;
	}

    /**
     * @param string $address
     * @return array
     */
	public function splitAdressString($searchStr='') {

	    $return = ['plz' => '', 'address' => '', 'string' => $searchStr];

	    if(strlen($searchStr) < 1) return $return;
	    $parts = explode(' ', $searchStr,2);

	    if(is_numeric($parts[0])) {
            $plz = $parts[0];
            $address = $parts[1];
        } else {
            $address = implode(' ',$parts);
        }

	    // plz pruefen und schreiben
        if(strlen($plz) === 5 && is_numeric($plz)) $return['plz'] = $plz;

        // adresse bereinigen und schreiben
        $address = trim( strip_tags( str_replace(',','',$address)));
        $return['address'] = $address;

        // suchstring bereinigen und schreiben
        $return['string'] = trim( strip_tags( $return['string']));

        return $return;
    }

	/**
	* get the distance from search-location and library
	* @param object
	* @return string
	*/
	public function getDistance($objLib)
	{
		$session = Session::getInstance()->get('bnfilter');
		$distance = '';

		if(strlen($session['geo_lat']) > 0 && strlen($session['geo_lon']) > 0 && strlen($session['distance']) > 0)
		{
			$resultObj = Database::getInstance()->prepare("SELECT 
				ACOS(SIN(RADIANS(lat)) * SIN(RADIANS(?)) + COS(RADIANS(lat)) * COS(RADIANS(?)) * COS(RADIANS(lon)- RADIANS(?))) * 6380 AS `distance` 
				FROM `tl_bn_libraries`
				WHERE `tl_bn_libraries`.`id` = ?
				AND ACOS( SIN(RADIANS(lat)) * SIN(RADIANS(?)) + COS(RADIANS(lat)) * COS(RADIANS(?)) * COS(RADIANS(lon) - RADIANS(?))) * 6380 <= ?")
			->limit(1)
			->execute(
					$session['geo_lat'], $session['geo_lat'], $session['geo_lon'],
					$objLib->id,
					$session['geo_lat'], $session['geo_lat'], $session['geo_lon'],$session['distance']
				);

			if($resultObj->numRows > 0) $distance = $resultObj->distance;
		}

		return $distance;
	}

	/**
	* pruft ob eine Bibliothek die gesuchte Leistung anbietet
	* @param object
	* @param integer
	* @return bool
	*/
	public function hasLeistung($objLib)
	{
		$session = Session::getInstance()->get('bnfilter');
		$has = false;
		$libLeistungenArr = array();

		if(strlen($objLib->leistungen)>0)$libLeistungenArr = unserialize($objLib->leistungen);
		if(in_array($session['leistungen'], $libLeistungenArr)) $has = true;

		return $has;
	}

	/**
	* pruft ob eine Bibliothek die gesuchte Medienart anbietet
	* @param object
	* @param integer
	* @return bool
	*/
	public function hasMedia($objLib)
	{
		$session = Session::getInstance()->get('bnfilter');
		$has = false;
		$libMedienArr = array();

		if(strlen($objLib->medien)>0) $libMedienArr = unserialize($objLib->medien);
		if(in_array($session['medien'], $libMedienArr)) $has = true;

		return $has;
	}

	public function getPoiImage($objLib){
	    $r = '';

	    $staticMap = new OsmStaticMaps();
        $mapOptions = $staticMap->getOptions();

        $filename = $objLib->id.$mapOptions['format'];

        // wenn das Ordner anlegen nicht klappt leer zurück
        if(!$this->checkOrCreateDir(TL_ROOT.'/'.$GLOBALS['BN']['BN_IMAGE_PATH'])) return $r;

        $filepath = $GLOBALS['BN']['BN_IMAGE_PATH'].'/'.$filename;

        // wenn die Datei bereits existiert den Pfad zurückgeben
        if(file_exists(TL_ROOT.'/'.$filepath)) return $filepath;

        //set Bibliothek-Data for map
        $mapOptions['marker_coordinates'] = $objLib->lon.','.$objLib->lat;
        $mapOptions['coordinates'] = $objLib->lon.','.$objLib->lat;
        $mapOptions['zoom'] = 13;

        return $staticMap->getAndSaveStaticMapImage($filepath, $mapOptions);
    }

    /**
     * @param string $dirPath
     * @return bool
     */
    public static function checkOrCreateDir($dirPath='') {

	    if(strlen($dirPath) < 1) return false;

	    // ToDo try-catch
        if (!is_dir($dirPath) )
        {
            Files::getInstance()->mkdir($dirPath);
        }

        return (is_dir($dirPath))? true : false;
    }

}
