<?php

/*
 * This file is part of bn-libraries-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */

/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Srhinow\BnLibrariesBundle\Modules;


use Contao\Image;
use Contao\StringUtil;
use Srhinow\BnLibrariesBundle\Models\BnLibrariesModel;

/**
 * Class ModuleBnSearchList
 */
class ModuleBnDetails extends ModuleBn
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_bn_details';


	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new \BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### BIBLIOTHEK-DETAILS ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		// Set the item from the auto_item parameter
		if (!isset($_GET['lib']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
		{
			\Input::setGet('lib', \Input::get('auto_item'));
		}

		// Do not index or cache the page if no news item has been specified
		if (!\Input::get('lib'))
		{
			global $objPage;
			$objPage->noSearch = 1;
			$objPage->cache = 0;
			return '';
		}

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{

		global $objPage;

		$session = $this->Session->get('bnfilter')?: array();

		// Get the total number of items
		$objLibrary = BnLibrariesModel::findLibByIdOrAlias(\Input::get('lib'));

		if ($objLibrary === null)
		{
			// Do not index or cache the page
			$objPage->noSearch = 1;
			$objPage->cache = 0;

			// Send a 404 header
			header('HTTP/1.1 404 Not Found');
			$this->Template->articles = '<p class="error">' . sprintf($GLOBALS['TL_LANG']['MSC']['invalidPage'], \Input::get('items')) . '</p>';
			return;
		}

		// zum sammeln fuers template
		$libData = $objLibrary->row();

		//Static-Map-Image holen /speichern falls noch nicht auf dem Server gespeichert
        $libData['poiImage'] = $this->getPoiImage($objLibrary);

		// Leitung
		if((int) $libData['leitung'] > 0 )
		{
			$leitungObj = $this->Database->prepare('SELECT * FROM `tl_bn_leitung` WHERE `id`=? ')
			->limit(1)
			->execute($libData['leitung']);

			if($leitungObj->numRows > 0) $libData['leitung'] = $leitungObj->name;
		}

		// Traeger
		if((int) $libData['traeger'] > 0 )
		{
			$leitungObj = $this->Database->prepare('SELECT * FROM `tl_bn_traeger` WHERE `id`=? ')
			->limit(1)
			->execute($libData['traeger']);

			if($leitungObj->numRows > 0) $libData['traeger'] = $leitungObj->name;
		}

		// Website
		 $libData['website_href'] = (substr($libData['website'],0,4) != 'http') ? 'http://'.$libData['website'] : $libData['website'];

		// webkatalog
		$libData['webkatalog_href'] = (substr($libData['webkatalog'],0,4) != 'http') ? 'http://'.$libData['webkatalog'] : $libData['webkatalog'];

		// onleihe
		$libData['onleihe_href'] = (substr($libData['onleihe'],0,4) != 'http') ? 'https://'.$libData['onleihe'] : $libData['onleihe'];

		// facebook
		$libData['facebook_href'] = (substr($libData['facebook'],0,4) != 'http') ? 'https://'.$libData['facebook'] : $libData['facebook'];

		// twitter
		$libData['twitter_href'] = (substr($libData['twitter'],0,4) != 'http') ? 'https://'.$libData['twitter'] : $libData['twitter'];

		// gplus
		$libData['gplus_href'] = (substr($libData['gplus'],0,4) != 'http') ? 'https://'.$libData['gplus'] : $libData['gplus'];

		// blog
		$libData['blog_href'] = (substr($libData['blog'],0,4) != 'http') ? 'https://'.$libData['blog'] : $libData['blog'];

        // instagramm
        $libData['instagram_href'] = (substr($libData['instagram'],0,4) != 'http') ? 'https://'.$libData['instagram'] : $libData['instagram'];

        // Email
//		$this->import('String');
		$libData['email'] = StringUtil::encodeEmail($libData['email']);

		//for smartphone phonenumber clean for href
		$replaces = array(' ', ',','/','(',')','[',']','{','}','+');
		$libData['phone_href'] = str_replace($replaces,'',$libData['telefon']);

		// Open-Status
		$libData['open_status'] = $this->getCurrentOpenStatus($objLibrary);
//		$GLOBALS['TL_JAVASCRIPT'][] = '.'.BN_PATH.'/assets/js/bn_fe.js';

		// Google-Maps url-search-string
		$libData['gmapsplace'] = ampersand($libData['strasse'].' '.$libData['hausnummer'].', '.$libData['plz'].' '.$libData['ort'].', Niedersachsen');

		// media
		$medienIds = unserialize($libData['medien']);
		if(is_array($medienIds) && count($medienIds)>0)
		{
			$medienArr = array('Bücher');

			$medienObj = $this->Database->prepare("SELECT * FROM `tl_bn_medien` WHERE id IN(".implode(',', array_map('intval', $medienIds)).")")->execute();

			if($medienObj->numRows > 0)
			{
				while($medienObj->next()) $medienArr[] = $medienObj->name;
				$libData['medien'] = $medienArr;
			}
		}

		// leistungen
		$leistungenIds = unserialize($libData['leistungen']);
		if(is_array($leistungenIds) && count($leistungenIds)>0)
		{
			$leistungenArr = array();

			$leistungenObj = $this->Database->prepare("SELECT * FROM `tl_bn_leistungen` WHERE id IN(".implode(',', array_map('intval', $leistungenIds)).")")->execute();

			if($leistungenObj->numRows > 0)
			{
				while($leistungenObj->next()) $leistungenArr[] = $leistungenObj->name;
				$libData['leistungen'] = $leistungenArr;
			}
		}
		
		//Bilder vorbereiten
		$scaletype = 'proportional';
		$img_height = 250;
		$img_width = 250;
		$libData['images'] = array();
		for($ic = 0; $ic <= 5; $ic++)
		{
			if(strlen($libData['image_'.$ic]) > 0) 
			{
				$libData['images'][] = array
				(
					'thumb' => Image::get($libData['image_'.$ic],$img_width, $img_height,$scaletype),
					'src' => $libData['image_'.$ic]
				);
			}
		}

		$this->Template->data = $libData;
		$this->Template->articles = '';
		$this->Template->referer = 'javascript:history.go(-1)';
		$this->Template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];
	}
}
