<?php

/*
 * This file is part of bn-libraries-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */

/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Srhinow\BnLibrariesBundle\Modules;



use Contao\BackendTemplate;
use Contao\Input;
use Contao\PageModel;
use Contao\Session;
use Srhinow\BnLibrariesBundle\Models\BnLibrariesModel;

/**
 * Class ModuleBnSearchRegion
 */
class ModuleBnSearchForm extends ModuleBn
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_bn_search_form';


	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### BIBLIOTHEK-REGION-SUCHE ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		// Ajax Requests abfangen
		if(Input::get('id') && $this->Environment->get('isAjaxRequest')){
		     $this->generateAjax();
		     exit;
		}

        // template overwrite
		if (strlen($this->mod_bn_template)) $this->strTemplate = $this->mod_bn_template;

		// Set the item from the auto_item parameter
		if (!isset($_GET['s']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
		{
			Input::setGet('s', Input::get('auto_item'));
		}

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{
		$this->import('FrontendUser','User');

		$this->Template->session = false;
		$session = $this->Session->get('bnfilter')?: array();

		//Formular verarbeiten wenn es gesendet wurde
		if(Input::post('FORM_SUBMIT') == 'tl_bn_search_form')
		{
			if(Input::post('filter_reset') == 1)
			{
				$session = array();

			}
			else
			{
				$geodata = $this->getGeoDataFromCurrentPosition(Input::post('plzcity'));

				$session = array
				(
					'plzcity' => $geodata['plzcity'],
					'distance' => Input::post('distance'),
					'only_open' => Input::post('only_open'),
					'leistungen' => Input::post('leistungen'),
					'medien' => Input::post('medien'),
					'geo_lat' => $geodata['lat'],
					'geo_lon' => $geodata['lon']
				);

			}

			Session::getInstance()->set('bnfilter', $session);
			Input::setPost('FORM_SUBMIT','');

			if($this->jumpTo)
			{
				$objDetailPage = PageModel::findByPk($this->jumpTo);
				$listUrl = ampersand( $this->generateFrontendUrl($objDetailPage->row()) );
				$this->redirect($listUrl);
			}
			$this->reload();
		}

		if(array_key_exists('geo_lat', $session) && array_key_exists("geo_lon", $session))
		{
			$geodata = array
			(
				'lat'=>$session['geo_lat'],
				'lon'=>$session['geo_lon']
			);
		}
		else
		{
			$geodata = $this->getGeoDataFromCurrentPosition();
		}

		// Get the total number of items
        $intTotal = 0;
        if(isset($session) && is_array($session) && array_key_exists('distance',$session)) {
            $intTotal = BnLibrariesModel::countLibEntries($geodata,$session['distance']);
        }

		// Filter anwenden um die Gesamtanzahl zuermitteln
		if($intTotal > 0)
		{
			$filterLibsObj = BnLibrariesModel::findLibs($intTotal, 0, $geodata, $session['distance']);

			$counter = 0;
			$idArr = array();

			while($filterLibsObj->next())
			{
				// aktuell offen
				if($session['only_open'] && $this->getCurrentOpenStatus($filterLibsObj) != 'open') continue;
				// bietet eine bestimmte Leistung an
				if(strlen($session['leistungen']) > 0 && !$this->hasLeistung($filterLibsObj)) continue;
				// bietet eine bestimmte Medienart an
				if(strlen($session['medien']) > 0 && !$this->hasMedia($filterLibsObj)) continue;

				//wenn alle Filter stimmen -> Werte setzen
				$idArr[] = $filterLibsObj->id;
				$counter++;
			}
			if((int) $intTotal > $counter) $intTotal = $counter;
		}
		$this->Template->total = (int) $intTotal;

		if(count($session) > 0)
		{
			$this->Template->session = true;
			$this->Template->plzcity = $session['plzcity'];
			$this->Template->distance = $session['distance'];
			$this->Template->only_open = $session['only_open'];
			$this->Template->lid = $session['leistungen'];
			$this->Template->mid = $session['medien'];
		}
		else
		{
			//default-Werte setzen
			$this->Template->distance = 15;
		}

		$GLOBALS['TL_CSS'][] = 'bundles/srhinowbnlibraries/css/jquery-ui-autocomplete.min.css';
		$GLOBALS['TL_CSS'][] = 'bundles/srhinowbnlibraries/css/bn_autocomplete.css';
		$GLOBALS['TL_JAVASCRIPT'][] = 'bundles/srhinowbnlibraries/js/jquery-ui-autocomplete.min.js';
		$GLOBALS['TL_BODY'][] = '<script>
		(function($){

			$(document).ready(function(){

			    $("#plzcity").autocomplete({
			        source: document.URL+"?action=ajax&id='.$this->id.'",
			        minLength: 2,
			        response: function( event, ui ) {
			        	console.log(ui.content);
			        }
			    });

			});

		})(jQuery);
		</script>';

		//Leistungen-Optionen
		$leistungenArr = array();
		$lObj = $this->Database->prepare('SELECT `id`,`name` FROM `tl_bn_leistungen` ORDER BY `sorting` ASC')->execute();
		if($lObj->numRows > 0)
		{
			while($lObj->next())
			{
				$leistungenArr[$lObj->id] = $lObj->name;
			}
		}
		$this->Template->leistungen = $leistungenArr;

		//Medien-Optionen
		$medienArr = array();
		$mObj = $this->Database->prepare('SELECT `id`,`name` FROM `tl_bn_medien` ORDER BY `sorting` ASC')->execute();
		if($mObj->numRows > 0)
		{
			while($mObj->next())
			{
				$medienArr[$mObj->id] = $mObj->name;
			}
		}
		$this->Template->medien = $medienArr;

	}

	public function generateAjax()
	{

		$plzort = trim($_GET['term']);

	   if((strlen($plzort) >= 2) && (strlen($plzort) <= 10))
	   {
			//prüfen ob es eine Zahl ist (plz)
			if (is_numeric($plzort))
			{
			    $resObj = $this->Database->prepare("SELECT `zc_zip`, `zc_location_name` FROM `zip_coordinates` WHERE `zc_zip` LIKE ?")
						  ->limit(50)
						  ->execute($plzort.'%');
			}
			else{
			    $resObj = $this->Database->prepare("SELECT `zc_zip`, `zc_location_name` FROM `zip_coordinates` WHERE `zc_location_name` LIKE ?")
						  ->limit(50)
						  ->execute($plzort.'%');

			}

			if ($resObj->numRows > 0)
			{
			    while($resObj->next())
			    {
			        $items[] = $resObj->zc_zip .' '. $resObj->zc_location_name;
			    }
			    $items[] = $_GET['term'];
			    header('Content-type: application/json');
			    echo json_encode($items);
			    exit();
			}
		}
	}

}
