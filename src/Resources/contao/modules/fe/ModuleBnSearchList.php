<?php

/*
 * This file is part of bn-libraries-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */

/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Srhinow\BnLibrariesBundle\Modules;


use Contao\BackendTemplate;
use Contao\Controller;
use Contao\FrontendTemplate;
use Contao\Input;
use Contao\PageModel;
use Contao\Pagination;
use Srhinow\BnLibrariesBundle\Models\BnLibrariesModel;

/**
 * Class ModuleBnSearchList
 */
class ModuleBnSearchList extends ModuleBn
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_bn_search_list';


	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### BIBLIOTHEK-SUCH-LISTE ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		// Set the item from the auto_item parameter
		if (!isset($_GET['s']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
		{
			Input::setGet('s', \Input::get('auto_item'));
		}

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{

		$offset = intval($this->skipFirst);
		$limit = null;
		$this->Template->libraries = array();

		// Maximum number of items
		if ($this->numberOfItems > 0)
		{
			$limit = $this->numberOfItems;
		}

		$session = $this->Session->get('bnfilter')?: array();

		if(strlen($session['geo_lat'])>0 && strlen($session['geo_lon'])>0)
		{
			$geodata = array
			(
				'lat'=>$session['geo_lat'],
				'lon'=>$session['geo_lon']
			);
		}
		else
		{
			$geodata = $this->getGeoDataFromCurrentPosition();
		}

		// Get the total number of items
		$intTotal = BnLibrariesModel::countLibEntries($geodata,$session['distance']);

		// Filter anwenden um die Gesamtanzahl zuermitteln
		if($intTotal > 0)
		{
			$filterLibsObj = BnLibrariesModel::findLibs($intTotal, 0, $geodata, $session['distance']);

			$counter = 0;
			$idArr = array();

			while($filterLibsObj->next())
			{
				// aktuell offen
				if($session['only_open'] && $this->getCurrentOpenStatus($filterLibsObj) != 'open') continue;
				// bietet eine bestimmte Leistung an
				if(strlen($session['leistungen']) > 0 && !$this->hasLeistung($filterLibsObj)) continue;
				// bietet eine bestimmte Medienart an
				if(strlen($session['medien']) > 0 && !$this->hasMedia($filterLibsObj)) continue;

				//wenn alle Filter stimmen -> Werte setzen
				$idArr[] = $filterLibsObj->id;
				$counter++;
			}
			if((int) $intTotal > $counter) $intTotal = $counter;
		}

		if ((int) $intTotal < 1)
		{
			$this->Template = new \FrontendTemplate('mod_bnentries_empty');
			$this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyBnList'];
			return;
		}

		$total = $intTotal - $offset;

		// Split the results
		if ($this->perPage > 0 && (!isset($limit) || $this->numberOfItems > $this->perPage))
		{


			// Adjust the overall limit
			if (isset($limit))
			{
				$total = min($limit, $total);
			}

			// Get the current page
			$id = 'page_n' . $this->id;
			$page = \Input::get($id) ?: 1;

			// Do not index or cache the page if the page number is outside the range
			if ($page < 1 || $page > max(ceil($total/$this->perPage), 1))
			{
				global $objPage;

				$objPage->noSearch = 1;
				$objPage->cache = 0;

				$objTarget = \PageModel::findByPk($objPage->id);
				if ($objTarget !== null)
				{
					$reloadUrl = ampersand(PageModel::getFrontendUrl( $objTarget->row() ) );
				}

				$this->redirect($reloadUrl);
			}

			// Set limit and offset
			$limit = $this->perPage;
			$offset += (max($page, 1) - 1) * $this->perPage;
			$skip = intval($this->skipFirst);

			// Overall limit
			if ($offset + $limit > $total + $skip)
			{
				$limit = $total + $skip - $offset;
			}

			// Add the pagination menu
            $objPaginationTemplate = new FrontendTemplate('pagination_bs');
			$objPagination = new Pagination($total, $this->perPage, $GLOBALS['TL_CONFIG']['maxPaginationLinks'], $id,$objPaginationTemplate);
			$this->Template->pagination = $objPagination->generate("\n  ");
		}


		// Get the items
		if (isset($limit))
		{

			$libsObj = BnLibrariesModel::findLibs($limit, $offset, $geodata, $session['distance'], $idArr);
		}
		else
		{

			$libsObj = BnLibrariesModel::findLibs(0, $offset, $geodata, $session['distance'], $idArr);
		}

		// No items found
		if ($libsObj === null)
		{
			$this->Template = new FrontendTemplate('mod_newsarchive_empty');
			$this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyList'];
		}
		else
		{
			$this->Template->libs = $this->parseLibraries($libsObj);
		}

//        $GLOBALS['TL_JAVASCRIPT'][] = PUBLIC_SRC_PATH.'/js/bn_fe.js';

		$this->Template->filterActive = Input::get('s') ? true : false;
		$this->Template->totalItems = $intTotal;


	}
}
