<?php
/**
 * Created by bn-libraries-bundle.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 01.12.18
 */

/**
 * -------------------------------------------------------------------------
 * MODULE CONSTANTS
 * -------------------------------------------------------------------------
 */
@define('BN_VERSION', '1.0');
@define('BN_BUILD', '1');
@define('BN_PATH', '/vendor/srhinow/bn-libraries-bundle');

$GLOBALS['BN']['BN_IMAGE_PATH'] = 'files/Bibliotheken';
$GLOBALS['BN']['BN_IMAGE_UPLOAD_TYPES'] = array('jpg','jpeg');
/**
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */
$bnBE = array
(
    'bn' => array
    (
        'libraries' => array
        (
            'tables' => array('tl_bn_libraries'),
            'stylesheet' => 'bundles/srhinowbnlibraries/css/be.css',
            'icon'  => 'bundles/srhinowbnlibraries/icons/entity.png',
            'csvLibraryExport' => array('beCSVExport', 'csvLibraryExport')
        ),
        'tl_bn_setup' => array
        (
            'callback'	=> 'ModuleBNSetup',
            'tables'	=> array(),
            'href'      => '',
            'icon'		=> 'bundles/srhinowbnlibraries/icons/process.png',
            'stylesheet' => 'bundles/srhinowbnlibraries/css/be.css',
        )
    )
);
array_insert($GLOBALS['BE_MOD'], 1, $bnBE);

/**
 * -------------------------------------------------------------------------
 * Setup Modules
 * -------------------------------------------------------------------------
 */
$GLOBALS['BN_SETUP_MOD'] = array
(
    'config' => array
    (
        'bn_settings' => array
        (
            'tables'					=> array('tl_bn_settings'),
            'href'						=> '&table=tl_bn_settings&act=edit&id=1',
            'icon'						=> 'bundles/srhinowbnlibraries/icons/process.png',
        ),
        'bn_traeger' => array
        (
            'tables'					=> array('tl_bn_traeger'),
            'icon'						=> 'bundles/srhinowbnlibraries/icons/asterisk_orange.png',
        ),
        'bn_leistungen' => array
        (
            'tables'					=> array('tl_bn_leistungen'),
            'icon'						=> 'bundles/srhinowbnlibraries/icons/award_star_bronze_1.png',
        ),
        'bn_medien' => array
        (
            'tables'					=> array('tl_bn_medien'),
            'icon'						=> 'bundles/srhinowbnlibraries/icons/cd.png',
        ),
    )
);

/**
 * -------------------------------------------------------------------------
 * Front END MODULES
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['FE_MOD'], 2, array
(
    'bn_libs' => array
    (
        'bn_search_form'    => 'Srhinow\BnLibrariesBundle\Modules\ModuleBnSearchForm',
        'bn_search_list'    => 'Srhinow\BnLibrariesBundle\Modules\ModuleBnSearchList',
        'bn_search_map'    => 'Srhinow\BnLibrariesBundle\Modules\ModuleBnSearchMap',
        'bn_details'    => 'Srhinow\BnLibrariesBundle\Modules\ModuleBnDetails',
        'bn_edit_entry'    => 'Srhinow\BnLibrariesBundle\Modules\ModuleBnEditEntry',
    )
));

/**
 * -------------------------------------------------------------------------
 * Models
 * -------------------------------------------------------------------------
 */

$GLOBALS['TL_MODELS']['tl_bn_leistungen'] = \Srhinow\BnLibrariesBundle\Models\BnLeistungenModel::class;
$GLOBALS['TL_MODELS']['tl_bn_libraries'] = \Srhinow\BnLibrariesBundle\Models\BnLibrariesModel::class;
$GLOBALS['TL_MODELS']['tl_bn_leitung'] = \Srhinow\BnLibrariesBundle\Models\BnLeitungModel::class;
$GLOBALS['TL_MODELS']['tl_bn_medien'] = \Srhinow\BnLibrariesBundle\Models\BnMedienModel::class;
$GLOBALS['TL_MODELS']['tl_bn_settings'] = \Srhinow\BnLibrariesBundle\Models\BnSettingsModel::class;
$GLOBALS['TL_MODELS']['tl_bn_traeger'] = \Srhinow\BnLibrariesBundle\Models\BnTraegerModel::class;
/**
 * -------------------------------------------------------------------------
 * HOOKS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_HOOKS']['createNewUser'][] = array('srhinow_bnlibraries.listener.new_user','sendNewRegisterNotification');
$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('srhinow_bnlibraries.listener.insert_tags', 'bnReplaceInsertTags');

/**
 * -------------------------------------------------------------------------
 * Add permissions
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_PERMISSIONS'][] = 'bnlibrariess';
$GLOBALS['TL_PERMISSIONS'][] = 'bnlibrariesp';
