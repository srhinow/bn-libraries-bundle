<?php

/*
 * This file is part of bn-libraries-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */

namespace Srhinow\BnLibrariesBundle\EventListener;

use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Srhinow\BnLibrariesBundle\Models\BnLibrariesModel;

class InsertTagsListener
{
    /**
     * @var ContaoFrameworkInterface
     */
    private $framework;

    /**
     * Constructor.
     *
     * @param ContaoFrameworkInterface $framework
     */
    public function __construct(ContaoFrameworkInterface $framework)
    {
        $this->framework = $framework;
    }

    /**
     * replace bn-specific inserttag if get-paramter isset
     * bn::colname::alternative from objPage
     *
     * @param string $tag
     *
     * @return string|false
     */
    public function bnReplaceInsertTags($strTag)
    {
        if (substr($strTag,0,4) == 'bn::')
        {
            global $objPage;
            $split = explode('::',$strTag);
            if(!array($split)) {
                return $strTag;
            }

            //nur BN-spezifisch weitermachen wenn es auf Seiten ist wo der lib-Paramter existiert
            $libId = \Input::get('lib');
            if(!$libId && isset($split[2])) return $objPage->{$split[2]};

            $objLibrary = BnLibrariesModel::findLibByIdOrAlias($libId);

            switch($split[1]){
                case 'fullname':

                    $fullname = $objLibrary->bibliotheksname;
                    if(strlen($objLibrary->zweigstellenname) > 0) $fullname .= ', '.$objLibrary->zweigstellenname;
                    return $fullname;
                    break;
                case 'printbutton':

                    return (!$libId) ? '' : '<a href="javascript:window.print()" class="printbutton"><i class="fa fa-print"></i></a>';
                    break;
                default:
                    return $objLibrary->$split[1];
            }
        }
        return false;
    }
}
