<?php

/*
 * This file is part of bn-libraries-bundle.
 *
 * @copyright  Sven Rhinow 2018 <https://www.sr-tag.de>
 *
 * @license LGPL-3.0+
 */

namespace Srhinow\BnLibrariesBundle\EventListener;

use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Contao\Email;
use Srhinow\BnLibrariesBundle\Models\BnSettingsModel;

class NewUserListener
{
    /**
     * @var ContaoFrameworkInterface
     */
    private $framework;

    /**
     * Constructor.
     *
     * @param ContaoFrameworkInterface $framework
     */
    public function __construct(ContaoFrameworkInterface $framework)
    {
        $this->framework = $framework;
    }

    /**
     * Send an admin notification e-mail
     * @param integer
     * @param array
     */
    public function sendNewRegisterNotification($intId, $arrData)
    {
        //get settings from bbk_properties
        $settingsObj = BnSettingsModel::findFirstSettings();

        if(null !== $settingsObj)
        {
            $objEmail = new Email();

            $objEmail->from = $GLOBALS['TL_ADMIN_EMAIL'];
            $objEmail->fromName = $GLOBALS['TL_ADMIN_NAME'];
            $objEmail->subject = sprintf($settingsObj->newuser_subject, $this->Environment->host);

            $strData = "\n\n";

            // Add user details
            foreach ($arrData as $k=>$v)
            {
                if ($k == 'password' || $k == 'tstamp' || $k == 'activation')
                {
                    continue;
                }

                $v = deserialize($v);

                if ($k == 'dateOfBirth' && strlen($v))
                {
                    $v = $this->parseDate($GLOBALS['TL_CONFIG']['dateFormat'], $v);
                }

                $strData .= $GLOBALS['TL_LANG']['tl_member'][$k][0] . ': ' . (is_array($v) ? implode(', ', $v) : $v) . "\n";
            }

            $objEmail->html = sprintf($settingsObj->newuser_html_email, $intId, $strData . "<br>");
            $objEmail->text = sprintf($settingsObj->newuser_text_email, $intId, $strData . "\n") . "\n";
            if(strlen($settingsObj->send_to_bcc) > 0 ) $objEmail->sendBcc($settingsObj->send_to_bcc);
            $objEmail->sendTo($settingsObj->send_to);
        }

        $logger = static::getContainer()->get('monolog.logger.contao');
        $level = LogLevel::INFO;
        $strText = 'A new user (ID ' . $intId . ') has registered on the website';
        $strCategory = TL_ACCESS;
        $strFunction = 'ModuleRegistration sendAdminNotification()';

        $logger->log($level, $strText, array('contao' => new ContaoContext($strFunction, $strCategory)));
    }
}
